create table thread ( 
    idt INT PRIMARY KEY AUTO_INCREMENT, 
    threads INT, durability VARCHAR(255), 
    color VARCHAR(255));

insert into thread (threads, durability, color) 
    values (100, '2 years', 'red'),(120, '3 years', 'black'), 
    (200, '5 years', 'blue'), (150, '1 year', 'green');

alter table thread add column img_filename varchar(255);

CREATE TABLE `user` (
  `uid` int NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_role` int NOT NULL,
  `last_login` datetime DEFAULT NULL,
  PRIMARY KEY (`uid`)
);

