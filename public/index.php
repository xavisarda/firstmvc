<?php

require_once(__DIR__.'/../app/inc/constants.php');
require_once(__DIR__.'/../app/controller/IndexController.php');

session_start();

$cnt = new IndexController();
$fs = $cnt->listFabrics();

?><html>
  <head>
    <title>Sample MVC</title>
  </head>
  <body>
    <h1>Sample MVC</h1>
    <?php if(isset($_SESSION[SESS_UNAME]) && $_SESSION[SESS_UNAME] != NULL && $_SESSION[SESS_UNAME] != "" ) { ?>
      <p>Welcom back, <?=$_SESSION[SESS_UNAME]?></p>-<a href="/forms/logout.php">Logout</a>
    <?php } else { ?>
      <form method="post" action="/forms/login.php">
        <dl>
            <dt><label for="addu-name">Username</label></dt>
            <dd><input type="text" id="addu-name" name="uname" tabindex="1"/></dd>
            <dt><label for="addu-pass">Password</label></dt>
            <dd><input type="text" id="addu-pass" name="upass" tabindex="2"/></dd>
            <dt>&nbsp;</dt>
            <dd><input type="submit" value="Login" name="usub"/></dd>
        </dl>
        </form>
    <?php } ?>
    <a href="/add.php">Add thread</a>
    <a href="/addUser.php">Add User</a>
    <ul>
        <?php foreach($fs as $f){ ?>
            <li>
              <a href="/details.php?index=<?=$f->getId()?>">
                <?=$f->getThreads()?>-<?=$f->getDurability()?>-<?=$f->getColor()?>
              </a>
              <?php if(isset($_SESSION[SESS_ROLE]) && $_SESSION[SESS_ROLE] != NULL && ($_SESSION[SESS_ROLE] == UROLE_ADMIN || $_SESSION[SESS_ROLE] == UROLE_USER)){ ?> 
              <a href="/update.php?index=<?=$f->getId()?>">Update</a>
              <?php } ?>
              <?php if(isset($_SESSION[SESS_ROLE]) && $_SESSION[SESS_ROLE] != NULL && $_SESSION[SESS_ROLE] == UROLE_ADMIN){ ?> 
              <a href="/delete.php?index=<?=$f->getId()?>">Delete</a>
              <?php } ?>
            </li>
        <?php } ?>
    </ul>
  </body>
</html>