<?php

require_once(__DIR__.'/../../app/inc/constants.php');
session_start();

$_SESSION[SESS_UNAME] = NULL;
$_SESSION[SESS_ROLE] = NULL;

header('Location: /');
