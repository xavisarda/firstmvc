<?php

class Fabric{

    private $_threads;
    private $_durability;
    private $_color;
    private $_image;
    private $_id;

    public function __construct($t = 100, $d = '1 year', $c = "No color", $img = null, $i = null){
        $this->setColor($c);
        $this->setDurability($d);
        $this->setThreads($t);
        $this->setId($i);
        $this->setImage($img);
    }

    public function getId(){
        return $this->_id;
    }

    public function getThreads(){
        return $this->_threads;
    }

    public function getDurability(){
        return $this->_durability;
    }

    public function getColor(){
        return $this->_color;
    }

    public function getImage(){
        return $this->_image;
    }

    public function setId($v){
        $this->_id = $v;
    }

    public function setThreads($v){
        $this->_threads = $v;
    }

    public function setDurability($v){
        $this->_durability = $v;
    }

    public function setColor($v){
        $this->_color = $v;
    }

    public function setImage($v){
        $this->_image = $v;
    }

}
